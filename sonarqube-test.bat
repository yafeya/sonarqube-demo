@echo off

echo "step1: enter snp-category..."
cd c:\src\snp-category
rmdir /s /q TestResults

echo "step2: sonarqube begin..."
dotnet "C:\Program Files\sonar-scanner\sonar-scanner-net5.0\SonarScanner.MSBuild.dll" begin /k:"snp-category-cs" /d:sonar.login="e7249dec68c4114d438278fa8ac64ef185a69da2" /d:sonar.host.url="https://yafeya.japaneast.cloudapp.azure.com" /d:sonar.language="cs" /d:sonar.exclusions="**/*.css,**/*.scss,**/*.less,**/*.dll,**/*.config,**/*.nuspec,**/*.js,**/*Test*.cs,**/*.cshtml,**/*.html" /d:sonar.cs.vscoveragexml.reportsPaths="./TestResults/**"

echo "step3: build the solution..."
dotnet build ./src/SnpCategory.sln

echo "step4: execute all the unit-tests..."
vstest.console.exe /EnableCodeCoverage ".\src\Tests\UnitTests\Assembler.UnitTests\bin\Debug\netcoreapp3.1\Dell.SnpCategoryModule.Assembler.UnitTests.dll"
vstest.console.exe /EnableCodeCoverage ".\src\Tests\UnitTests\DomainSupport.UnitTests\bin\Debug\netcoreapp3.1\Dell.SnpCategoryModule.DomainSupport.UnitTests.dll"
vstest.console.exe /EnableCodeCoverage ".\src\Tests\UnitTests\Web.UnitTests\bin\Debug\netcoreapp3.1\Dell.SnpCategoryModule.Web.UnitTests.dll"

echo "step5: convert to code-coverage report..."

for /f "delims=" %%f in ('forfiles /s /m *.coverage /c "cmd /c echo @relpath"') do (
	echo handling about %%~nxf
	set outputFile = %%~dpfoutput.coveragexml
	CodeCoverage.exe analyze /output:"%%~dpfoutput.coveragexml" %%f
)

echo "Step6: end sonarqube..."
dotnet "C:\Program Files\sonar-scanner\sonar-scanner-net5.0\SonarScanner.MSBuild.dll" end /d:sonar.login="e7249dec68c4114d438278fa8ac64ef185a69da2"

echo "Step7: back to ~/ ..."
cd c:\users\feiyang_yang\desktop